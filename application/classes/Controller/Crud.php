<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Crud extends Controller {

	public function action_index()
	{
    	$members = DB::select()
	            ->from('fileupload')
	            // ->order_by('id', 'DESC')
	            ->execute()->as_array('id');
	            $this->response->body(View::factory('crud')->bind('data',$members));

	}
	public function action_add()
	{
		$title 		= Arr::get($_POST, 'title');
		$thumbnail 	= Arr::get($_POST, 'thumbnail');
		$image 		= $_FILES['image'];
		// print_r($image);
		
		$date = date('Y-m-d');

		    $upload_dir = DOCROOT.'uploads/';
		    $file=Upload::save($image, NULL, $upload_dir);
		    
		      $filename = strtolower(Text::random('alnum', 20)).'.jpg';
            
          // resize image file to 210x210
            Image::factory($file)
                  ->resize(210, 210, Image::NONE)
                  ->save($upload_dir.$filename);

                   unlink($file);
          
		$query = DB::insert('fileupload', array('title', 'thumbnail','filename','added'))->values(array($title, $thumbnail,$filename,$date))->execute();

		$action = 'action_index';
        $this->$action();
	}
	public function action_edit()
	{
		$id = $this->request->param('id');
		$members = DB::select()
	            ->from('fileupload')
	            ->where('id', '=', $id)
	            ->execute()->as_array('id');
	            $this->response->body(View::factory('edit')->bind('data',$members[$id]));
	}
	public function action_delete()
	{
		$id 		= Arr::get($_POST, 'id');
		$filename 	= Arr::get($_POST, 'filename');
		// $id = $this->request->param('id');

		$query = DB::delete('fileupload')->where('id', '=',$id)->execute();

			$upload_dir = DOCROOT.'uploads/'.$filename;
			unlink($upload_dir);

		$response = array('status' => 200);
		echo json_encode($response);
	}
	public function action_editsave()
	{
		$id 		= Arr::get($_POST, 'id');
		$title 		= Arr::get($_POST, 'title');
		$thumbnail 	= Arr::get($_POST, 'thumbnail');
		
		if(isset($_FILES['image'])){
			$filename 	= Arr::get($_POST, 'filename');
			$upload_dir = DOCROOT.'uploads/'.$filename;
			unlink($upload_dir);

			$image 		= $_FILES['image'];

			 $upload_dir = DOCROOT.'uploads/';
		    $file=Upload::save($image, NULL, $upload_dir);
		    
		      $filename = strtolower(Text::random('alnum', 20)).'.jpg';
            
          // resize image file to 210x210
            Image::factory($file)
                  ->resize(210, 210, Image::NONE)
                  ->save($upload_dir.$filename);

                   unlink($file);
                   // echo "in if   ".$filename;
		}
		else{
			$filename 	= Arr::get($_POST, 'filename');
			// echo "in else  ".$filename;
		}
            

		$query = DB::update('fileupload')->set(array('title' => $title,'thumbnail'=> $thumbnail,'filename'=> $filename))->where('id', '=', $id)->execute();

		$post = array('title' => $title,'thumbnail'=> $thumbnail,'filename'=> $filename);
		// print_r($post);

		$response = array('status' => 200);
		echo json_encode($response);
		// $action = 'action_index';
  //       $this->$action();
	
		
	}
}
