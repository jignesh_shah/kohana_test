<!DOCTYPE html>
<html>
<head>
	<title>crud</title>
	 <?php
    print HTML::style('assets/css/styles.css');
    
    ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
</head>
<body>

	<div class="container">
		<div class="labelCenter"><h4>Hi this is my first kohana framwork tutorials</h4>
			<button type="button" class="addbtn" data-toggle="modal" data-target="#myModal">Add new</button>
		</div>
		<div>
			<table class="table_style" cellspacing="0">
				<tr>
					<th>ID</th>
					<th>title</th>
					<th>thumbnail</th>
					<th>filename</th>
					<th>added date</th>
					<th colspan="2">Actions</th>
				</tr>
				<?php 
				
				$count = 1;
				foreach($data as $users){
				 ?>
				<tr>
					<td><?php echo $count++ ?></td>
					<td><?php echo $users['title'] ?> </td>
					<td><?php echo $users['thumbnail'] ?></td>
					<td><img src="<?php echo URL::base() ?>uploads/<?php echo $users['filename'] ?>" style="width: 50px;"></td>
					<td><?php echo $users['added'] ?></td>
					<td>
						<a href="<?php echo URL::base() ?>index.php/Crud/edit/<?php echo $users['id'] ?>" >Edit</a>
					<a href="javascript:void(0);" onclick="delete1('<?php echo $users['id'] ?>','<?php echo $users['filename']?>')" >Delete</a>
				</td>
				</tr>
				<?php  }  ?>
			</table>
		</div>
		

		

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add New FIle</h4>
        </div>
        <div class="modal-body">
        	<form name="addform" action="index.php/Crud" method="post" enctype="multipart/form-data">
		        <div class="addbox">
		        	<p>Title</p>
		          	<input type="text" name="title" id="title">
	          	</div>
	          
	          	<div class="addbox">
		        	<p>Thumbnail</p>
		          	<input type="text" name="thumbnail" id="thumbnail">
	          	</div>
	          	<div class="addbox">
		        	<p>Filename</p>
		          	<input type="file" name="filename" id="filename">
	          	</div>
	          
        
      
        </form>
        </div>

          	<div class="modal-footer">
	          <button type="button" class="btn btn-default btn_edit" data-dismiss="modal" onclick="cancel();">Close</button>
	          <button type="button" class="btn btn-default btn_edit" data-dismiss="modal" onclick="add();">Add</button>
	          <!-- <button type="submit" class="btn btn-default" >dasdas</button> -->
        	</div>

      </div>
      
    </div>
  </div>




	</div>	
	<?php
  print HTML::script('assets/js/jquery.min.js');
  print HTML::script('assets/js/bootstrap.min.js');

  ?>
  <?php
  print HTML::script('assets/js/custom.js');
  ?>

<!-- <?php echo form::open('album/create'); ?> -->
<!--  <table class='editor'>
<tr>
    <td colspan='2' class='editor_title'>Create new album</td>
</tr>
<?php
//$genres_list  = ['asdasd','asdasda'];
    //echo "<tr>";
   // echo "<td>".form::label('name', 'Name: ')."</td>";
    // echo "<td>".form::input('name', '')."</td>";
    // echo "</tr>";
     
    // echo "<tr>";
    // echo "<td>".form::label('author', 'Author: ')."</td>";  
    // echo "<td>".form::input('author', '')."</td>";  
    // echo "<tr/>";
     
    // echo "<tr>";
    // echo "<td>".form::label('genre', 'Genre: ')."</td>";    
    // // echo "<td>".form::dropdown('genre_id',$genres_list)."</td>";
    // echo "<td>".form::input('author', '')."</td>";  
    // echo "<tr/>";
     
    // echo "<tr>";
    // echo "<td colspan='2' align='left'>".form::submit('submit', 'Create album')."</td>";
    // echo "</tr>";
?>
</table>  -->
<!-- <?php echo form::close(); ?> -->
<script type="text/javascript">
	
	var baseurl = "<?php echo URL::base() ?>";

	function click2(argument) {
		console.log(baseurl);
		$.cookie('baseurl',baseurl);
	}

	function add() {
   // var baseurl= $.cookie('baseurl');

    console.log(baseurl);
	var title = $("#title").val();
	var thumbnail = $("#thumbnail").val();

	var formData = new FormData();
	formData.append('title',title);
	formData.append('thumbnail',thumbnail);
	formData.append('image', $('input[type=file]')[0].files[0]); 


$.ajax({
    url: baseurl+'/index.php/Crud/add',
    data: formData,
    type: 'POST',
    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
    processData: false, // NEEDED, DON'T OMIT THIS
    // ... Other options like success and etc
    success:function(res){
    	console.log(res);
    	window.location.href = baseurl;
    }
});
}
	function cancel() {
	console.log(baseurl);
	window.location.href = baseurl;
}

function delete1(id,filename) {

	var formData = new FormData();
	formData.append('id',id);
	formData.append('filename',filename);

	$.ajax({
    url: baseurl+'/index.php/Crud/delete',
    data: formData,
    type: 'POST',
    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
    processData: false, // NEEDED, DON'T OMIT THIS
    // ... Other options like success and etc
    success:function(res){
    	console.log(res);
    	window.location.href = baseurl;
    }
});
}
	</script>
</body>
</html>