<!DOCTYPE html>
<html>
<head>
	<title>Edit form</title>
</head>
<body>
	<?php
	// print_r($data);

    print HTML::style('assets/css/styles.css');
    
	?>
	<div class="container">
			<div class="mainclasss">
				<div class="edit_title">
					<h1>Edit Record</h1>
				</div>
					<form name="editform" class="edit_form" enctype="multipart/form-data">
					        <div class="addbox">
					        	<input type="hidden" name="iss" id="iss" value="<?php echo $data['id'] ?>">
					        	<p>Title</p>
					          	<input type="text" name="title" id="title"  value="<?php echo $data['title'] ?>">
				          	</div>
				          
				          	<div class="addbox">
					        	<p>Thumbnail</p>
					          	<input type="text" name="thumbnail" id="thumbnail" value="<?php echo $data['thumbnail'] ?>">
				          	</div>
				          	<div class="addbox">
					        	<p>Filename</p>
					        	<img src="<?php echo URL::base() ?>uploads/<?php echo $data['filename'] ?>" style="width: 100px;">
					        	<input type="file" name="filename" id="filename">
					          	<!-- <input type="text" name="filename" id="filename" value="<?php echo $data['filename'] ?>"> -->
				          	</div>
				          
			        
			        <div class="edit-footer">
			          <button type="button" class="btn btn-default btn_edit" onclick="cancel();">Close</button>
			          <button type="button" class="btn btn-default btn_edit" onclick="save();">Save</button>
			          <!-- <button type="submit" class="btn btn-default" >dasdas</button> -->
			        </div>
			        
					</form>
			</div>
			
  <?php
  print HTML::script('assets/js/jquery.min.js');
  print HTML::script('assets/js/bootstrap.min.js');

  ?>
	</div>
	<script type="text/javascript">
		var baseurl = "<?php echo URL::base() ?>";
		function save() {
   // var baseurl= $.cookie('baseurl');

    console.log(baseurl);
	var title = $("#title").val();
	var thumbnail = $("#thumbnail").val();
	var file = "<?php echo $data['filename'] ?>";
	var formData = new FormData();
	var id = $("#iss").val();
	formData.append('title',title);
	formData.append('id',id);
	formData.append('thumbnail',thumbnail);
	formData.append('filename',file);
	formData.append('image', $('input[type=file]')[0].files[0]); 


$.ajax({
    url: baseurl+'/index.php/Crud/editsave',
    data: formData,
    type: 'POST',
    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
    processData: false, // NEEDED, DON'T OMIT THIS
    // ... Other options like success and etc
    success:function(res){
    	console.log(res);
    	window.location.href = baseurl;
    }
});
}

function cancel() {
	// console.log(baseurl);
	window.location.href = baseurl;
}
	</script>
</body>
</html>